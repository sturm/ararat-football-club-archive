<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Ararat Football Club</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="Ben Sturmfels" />
<meta name="description" content="Information, stats and history about the club and its players." />
<meta name="keywords" content="ararat football club, afc, ararat football, footy, ararat, football, ararat netball club, netball, stats, history, statistics, photos" />
<meta name="distribution" content="global" />
<link rel="stylesheet" title="Simple stuff for dud browsers" href="simple.css" type="text/css" media="screen" />
<style type="text/css">
<!--
@import "complex.css";
-->
</style>
<link rel="stylesheet" title="Print me up" type="text/css" media="print" href="print.css" />
</head>

<body bgcolor="#FFFFFF"  text="#000000" alink="#3366FF" vlink="#990066" link="#990066">
<!--Header table-->
<table id="masthead" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<!--Rat logo--> 
    <td width="89"><img src="images/ratlogo.gif" width="89" height="127" alt="Rats Logo" /></td><!--Banner--><td width="463"><img src="images/title-graphic.jpg" width="463" height="127" alt="Gason Rats" /></td><!--Section links--><td bgcolor="#ff0000" valign="top" width="99%">
<div class="section" align="right">Home</div>
<div class="section" align="right"><a href="seniors/index.html" title="Senior League - Firsts">Seniors</a></div>
<div class="section" align="right"><a href="reserves/index.html" title="Reserve League - Seconds">Reserves</a></div>
<div class="section" align="right"><a href="thirds/index.html" title="Thirds">3rds, </a><a href="fourths/index.html" title="Fourths">4ths</a></div>
<div class="section" align="right"><a href="history/index.html" title="The background &amp; developement of the Club">Club History</a></div>
<div class="section" align="right"><a href="netball/index.html" title="Ararat Netball Club">Netball</a></div>
</td>
  </tr>
  <!--Title graphic-->
  <tr bgcolor="#ff0000"> 
    <td colspan="3"><img src="images/ararat-footy-title.gif" width="380" height="30" alt="Ararat Football Club" /></td>
  </tr>
</table>
<!--Content table-->
<table width="100%" border="0" cellspacing="0" cellpadding="10">

  <tr>
	<!--Main content column-->
    <td id="contentcol" colspan="2" width="99%">
   <!-- <h2>Welcome</h2>-->
<p>This site looks back through the proud history of the Club. Over 180 detailed pages are on offer, patiently collated by dedicated club members <a href="spaldo.html">Michael Spalding</a> and Alan Winnell. Enjoy!</p>
      <table style="margin-left: auto; margin-right: auto; margin-bottom: 12px;">
	<tr>
	  <td valign="bottom"><a href="seniors/"><img src="images/seniorswhite.gif" border="0" alt="Guernsey" /></a><h3 style="text-align: center"><a href="seniors/">Seniors</a></h3></td>
	  <td valign="bottom"><a href="reserves/"><img src="images/reserveswhite.gif" border="0" alt="Guernsey" /></a><h3 style="text-align: center"><a href="reserves/">Reserves</a></h3></td>
	  <td valign="bottom"><a href="thirds/"><img src="images/thirdswhite.gif" border="0" alt="Guernsey" /></a><h3 style="text-align: center"><a href="thirds/">Thirds</a></h3></td>
	  <td valign="bottom"><a href="fourths/"><img src="images/fourthswhite.gif" border="0" alt="Guernsey" /></a><h3 style="text-align: center"><a href="fourths/">Fourths</a></h3></td>
	</tr>
      </table>
<table width="100%" border="0">
<tr>
<td class="highlights" width="50%" valign="top">
<h3>About the Club</h3>
<p>The Rats are passionate and fierce competitors in the <a href="http://www.wfl.asn.au">Wimmera Football League</a>.</p>
<p>The club is based in the country town of <a href="http://www.ararat.asn.au">Ararat</a>, in South-West Victoria, Australia.  Members of the club come from all around the Ararat district, making up one of the strongest teams in the region. Footy here, is a part of life.</p>
  <p class="reference">See <a href="history/">Club history</a> for more...</p>
</td>

<td class="quicknotes" width="50%" valign="top">
<h3>Quick notes</h3>
<ul>
<li>For ladders and other weekly info, visit the <a href="http://www.wfl.asn.au">Wimmera Football League</a></li>
<li><a href="faq.html">Common questions</a> about this site</li>
</ul>
</td>
</tr>
</table>
<p>Can you correct a mistake, fill in a gap? Do you have any photos or more information? <a href="mailto://spaldom@netconnect.com.au">Mick</a> would be glad to hear from you!</p>
<!-- Caption tag MUST be on the same line as image tag, otherwise IE will put in a small gap -->
<!--<div class="photo" align="left" style="width: 197px; float: left"><a href="images/oval.jpg"><img src="images/ovalmini.jpg" width="197" height="128" alt="Alexandra Oval: Home of the Rats" border="0" /></a><p class="caption">Home - Alexandra Oval <a href="images/oval.jpg">(+)</a></p></div>-->
    </td>
	<!--Right hand image column-->
    <td id="imagecol" width="251" valign="top"><img src="images/bigmenfly2.jpg" width="200" height="281" alt="The Big Men Fly" /><div class="colcaption" align="right">David Clark: High flyer</div></td>
  </tr>
  <!--Non-CSS border-->
  <tr class="retro">
    <td colspan="3" align="center">
		<br /><hr size="1" width="100%" />
	</td>
  </tr>
  <tr>
	<!--Sponsor footer column-->
    <td valign="middle" align="center" width="120">
<div class="sponsor" align="center">Primary sponsor:</div>
<a href="http://www.gason.com.au" title="A.F. Gason Pty. Ltd. - Design &amp; Engineering Manufacturers"><img src="images/gasonlogo.gif" width="108" height="56" border="0" vspace="5" hspace="5" alt="Gason Logo" /></a>
    </td>
	<!--Copyright footer--> 
    <td valign="middle" width="99%"><div class="footer">No material from this site may be reproduced without written permission from the <a href="mailto://info@afc.ararat.net.au">Ararat Football Club</a>.
<br />Website production by <a href="http://www.stumbles.id.au/design/">Ben</a></div>
    </td>
    <td width="194"><div class="webstds">This site is accessable to all browsers, but will look its best in one that supports <a href="http://www.webstandards.org/upgrade/" title="Web Standards Project">web standards</a>.</div></td>
  </tr>
</table>
</body>
</html>
