<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>AFC Seniors: Goalkickers of 2002</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="Ben Sturmfels" />
<meta name="distribution" content="local" />
<link rel="stylesheet" href="../../simple.css" type="text/css" />
<style type="text/css">
<!--
@import "../../complex.css";
@import "../../data.css";
@import "../../goals.css";
-->
</style>
<link rel="stylesheet" title="Print me up" type="text/css" media="print" href="../../print.css" />
</head>

<body bgcolor="#ffffff" text="#000000" alink="#3366ff" vlink="#990066" link="#990066">
<!--Header table-->
<table id="masthead" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <!--Rat logo--> 
    <td width="89"><img src="../../images/ratlogo.gif" width="89" height="127" alt="Rats Logo" /></td>
    <!--Banner-->
    <td width="463"><img src="../../images/title-graphic.jpg" width="463" height="127" alt="Gason Rats" /></td>
    <!--Section links-->
    <td bgcolor="#FF0000" valign="top" width="99%">
<div class="section" align="right"><a href="../../index.php" title="The main title page">Home</a></div>
<div class="section" align="right"><a href="../../seniors/index.html" title="Senior League - Firsts">Seniors</a></div>
<div class="section" align="right"><a href="../../reserves/index.html" title="Reserve League - Soconds">Reserves</a></div>
<div class="section" align="right"><a href="../../thirds/index.html" title="Thirds">3rds, </a><a href="../../fourths/index.html" title="Fourths">4ths</a></div>
<div class="section" align="right"><a href="../../history/index.html" title="The background &amp; developement of the Club">Club History</a></div>
<div class="section" align="right"><a href="../../netball/index.html" title="Ararat Netball Club">Netball</a></div>
</td>
  </tr>
  <!--Title graphic-->
  <tr bgcolor="#ff0000"> 
    <td colspan="3"><h1><img src="../../images/ararat-footy-title.gif" width="380" height="30" alt="Ararat Football Club" /></h1></td>
  </tr>
</table>
<!--Content table-->
<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <!--Menu column-->
    <td id="menucol" width="170" valign="top">
	  <div class="menu"><a href="../players/index.html">Past Players</a></div>
          <div class="menu"><a href="../capt-coach/index.html">Coach/Captain</a></div>
	  <div class="menu"><a href="../afc-bf/index.html">AFC Best &amp; Fairest</a></div>
	  <div class="menu"><a href="../wfl-bf/index.html">WFL Best &amp; Fairest</a></div>
          <div class="menuselected"><a href="../goals/index.html">Goalkickers</a></div>
<ul class="submenu">
<!--      <li><a href="sengoals2003.php">Forward one year</a></li>-->
      <li><a href="sengoals2001.php">Back one year</a></li>
</ul>	  <div class="menu"><a href="../goalkickers/index.html">Leading Goalkickers</a></div><div class="menu"><a href="../gf-teams/index.html">Grand Final Teams</a></div>
	  <div class="menu"><a href="../vfl-afl/index.html">VFL/AFL Players</a></div>
	  <div class="menu"><a href="../interleague/index.html">Interleague</a></div>
	  <div class="menu"><a href="../opposition/index.html">Opposition</a></div>	  <img src="../../images/spacer.gif" width="170" height="1" border="0" alt="&nbsp;" />
<br class="retro" />
<div class="note"><strong>Note:<br /></strong>
<?php  include 'listofopposition.php';
?>
</div>

    </td>
	<!--Main content column-->
    <td id="contentcol" colspan="2">
 <h2>Seniors</h2>
<h3><strong>Goal kickers</strong> for each round of <strong>2002</strong></h3>
<table id="data" border="1">
<tr class="roundsrow">
<th>&nbsp;</th>
<th class="roundh">Round:</th>
<th>1</th>
<th>2</th>
<th>3</th>
<th>4</th>
<th>5</th>
<th>6</th>
<th>7</th>
<th>8</th>
<th>9</th>
<th>10</th>
<th>11</th>
<th>12</th>
<th>13</th>
<th>14</th>
<th>15</th>
<th>16</th>
<th>sf</th>
<th>gf</th>
<th>&nbsp;</th>
</tr>

<tr class="opprow">
<th class="playerh">PLAYER</th>
<th class="opphead">Opponent:</th>
<th>hs</th>
<th>we</th>
<th>h</th>
<th>n</th>
<th>d</th>
<th>hu</th>
<th>s</th>
<th>mm</th>
<th>hs</th>
<th>we</th>
<th>h</th>
<th>n</th>
<th>d</th>
<th>hu</th>
<th>s</th>
<th>mm</th>
<th>we</th>
<th>we</th>
<th>&nbsp;</th>
</tr>

<tr class="or">
<td class="surname">CAMPIGLI</td>
<td class="firstname">Paul</td>
<td>3</td>
<td>1</td>
<td>4</td>
<td>4</td>
<td>6</td>
<td>&nbsp;</td>
<td>5</td>
<td>6</td>
<td>4</td>
<td>3</td>
<td>2</td>
<td>1</td>
<td>6</td>
<td>1</td>
<td>5</td>
<td>5</td>
<td>&nbsp;</td>
<td>2</td>
<td class="ptotalo">(58)</td>
</tr>

<tr class="er">
<td class="surname">MAY</td>
<td class="firstname">Rohan</td>
<td class="oc">6</td>
<td>1</td>
<td class="oc">4</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>4</td>
<td class="oc">3</td>
<td>5</td>
<td class="oc">4</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>8</td>
<td class="oc">3</td>
<td>3</td>
<td class="oc">3</td>
<td>3</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="ptotalo">(50)</td>
</tr>

<tr class="or">
<td class="surname">FRATIN</td>
<td class="firstname">Michael</td>
<td>&nbsp;</td>
<td>4</td>
<td>1</td>
<td>2</td>
<td>&nbsp;</td>
<td>5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>2</td>
<td>2</td>
<td>1</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>5</td>
<td>4</td>
<td>4</td>
<td>4</td>
<td class="ptotalo">(35)</td>
</tr>

<tr class="er">
<td class="surname">ANTONIO</td>
<td class="firstname">Glenn</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>4</td>
<td class="oc">1</td>
<td>5</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>5</td>
<td class="oc">5</td>
<td>&nbsp;</td>
<td class="oc">8</td>
<td>&nbsp;</td>
<td class="oc">3</td>
<td>3</td>
<td class="ptotalo">(34)</td>
</tr>

<tr class="or">
<td class="surname">BLIGH</td>
<td class="firstname">Dale</td>
<td>&nbsp;</td>
<td>1</td>
<td>3</td>
<td>3</td>
<td>4</td>
<td>2</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>4</td>
<td>2</td>
<td>&nbsp;</td>
<td>1</td>
<td>1</td>
<td>2</td>
<td>1</td>
<td class="ptotalo">(28)</td>
</tr>

<tr class="er">
<td class="surname">WILLIAMSON</td>
<td class="firstname">Allister</td>
<td class="oc">1</td>
<td>2</td>
<td class="oc">1</td>
<td>2</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">2</td>
<td>2</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">2</td>
<td>2</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">2</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>2</td>
<td class="ptotalo">(24)</td>
</tr>

<tr class="or">
<td class="surname">JACKSON</td>
<td class="firstname">Matthew</td>
<td>2</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>4</td>
<td>&nbsp;</td>
<td>1</td>
<td>2</td>
<td>3</td>
<td>&nbsp;</td>
<td>4</td>
<td>&nbsp;</td>
<td>2</td>
<td>2</td>
<td>2</td>
<td>&nbsp;</td>
<td>2</td>
<td>&nbsp;</td>
<td class="ptotalo">(24)</td>
</tr>

<tr class="er">
<td class="surname">JAMES</td>
<td class="firstname">Jason</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">4</td>
<td>1</td>
<td class="oc">5</td>
<td>&nbsp;</td>
<td class="oc">2</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">2</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(18)</td>
</tr>

<tr class="or">
<td class="surname">PATTON</td>
<td class="firstname">Danny</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>3</td>
<td>3</td>
<td>1</td>
<td>2</td>
<td>3</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>1</td>
<td>2</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(17)</td>
</tr>

<tr class="er">
<td class="surname">OLIVER</td>
<td class="firstname">David</td>
<td class="oc">1</td>
<td>4</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>3</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="ptotalo">(15)</td>
</tr>

<tr class="or">
<td class="surname">SHEA</td>
<td class="firstname">Tim</td>
<td>2</td>
<td>5</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>1</td>
<td>3</td>
<td>2</td>
<td>&nbsp;</td>
<td class="ptotalo">(14)</td>
</tr>

<tr class="er">
<td class="surname">CORBETT</td>
<td class="firstname">Nic</td>
<td class="oc">&nbsp;</td>
<td>3</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>2</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(10)</td>
</tr>

<tr class="or">
<td class="surname">McLOUGHLAN</td>
<td class="firstname">Paul</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>2</td>
<td>1</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td class="ptotalo">(7)</td>
</tr>

<tr class="er">
<td class="surname">WALTON</td>
<td class="firstname">Doug</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">4</td>
<td>&nbsp;</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(7)</td>
</tr>

<tr class="or">
<td class="surname">DOWIE</td>
<td class="firstname">Brett</td>
<td>&nbsp;</td>
<td>3</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(6)</td>
</tr>

<tr class="er">
<td class="surname">WARING</td>
<td class="firstname">Campbell</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>3</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(6)</td>
</tr>

<tr class="or">
<td class="surname">TUCKER</td>
<td class="firstname">Richard</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(6)</td>
</tr>

<tr class="er">
<td class="surname">BRYANT</td>
<td class="firstname">Ashley</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="oc">1</td>
<td>1</td>
<td class="oc">1</td>
<td>2</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(6)</td>
</tr>

<tr class="or">
<td class="surname">ALLEN</td>
<td class="firstname">Shaun</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>3</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(3)</td>
</tr>

<tr class="er">
<td class="surname">WARD</td>
<td class="firstname">Andrew</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(3)</td>
</tr>

<tr class="or">
<td class="surname">PITMAN</td>
<td class="firstname">Sam</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>2</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(2)</td>
</tr>

<tr class="er">
<td class="surname">EDWARDS</td>
<td class="firstname">Brett</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(2)</td>
</tr>

<tr class="or">
<td class="surname">DOHNT</td>
<td class="firstname">Mark</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(1)</td>
</tr>

<tr class="er">
<td class="surname">SMITH</td>
<td class="firstname">Gary</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(1)</td>
</tr>

<tr class="or">
<td class="surname">LESLIE</td>
<td class="firstname">Adam</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(1)</td>
</tr>

<tr class="er">
<td class="surname">KING</td>
<td class="firstname">Richard</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(1)</td>
</tr>

<tr class="or">
<td class="surname">EDWARDS</td>
<td class="firstname">Nigel</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(1)</td>
</tr>

<tr class="ttotalsrow">
<th>&nbsp;</th>
<th class="totalsh">Totals:</th>
<th>17</th>
<th>24</th>
<th>21</th>
<th>21</th>
<th>35</th>
<th>21</th>
<th>23</th>
<th>24</th>
<th>25</th>
<th>10</th>
<th>15</th>
<th>25</th>
<th>27</th>
<th>11</th>
<th>30</th>
<th>23</th>
<th>16</th>
<th>13</th>
<th>&nbsp;</th>
</tr>
</table>
        </td>	
  </tr>

<?php
  include '../../datafooter.php';
?>

</table>
</body>
</html>

